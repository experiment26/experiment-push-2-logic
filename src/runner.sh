#!/bin/bash

echo "bash runner"

DATAFILE=$1

if [ "$DATAFILE" == "" ]; then
	echo "parameter 1: path to data file missing"
	exit 1
fi

DATE=$(date --iso-8601=seconds)
echo -e "${DATE}\t${RANDOM}" >> $DATAFILE

echo "done."

